/* test_drv.c */



#include <linux/module.h>

#include <linux/init.h>

#include <linux/fs.h>

#include <linux/kernel.h>

#include <linux/slab.h>

#include <linux/types.h>

#include <linux/errno.h>

#include <linux/cdev.h>

#include <asm/uaccess.h>

#define     TEST_DEVICE_NAME    "test_dev"

#define     BUFF_SZ         1024



static struct cdev test_dev;

unsigned int major =0;

static char *data = NULL;



/*º¯ÊýÉùÃ÷*/

static ssize_t test_read(struct file *file, char *buf, size_t count, loff_t *f_pos);

static ssize_t test_write(struct file *file,const char *buffer, size_t count,loff_t *f_pos);

static int test_open(struct inode *inode, struct file *file);

static int test_release(struct inode *inode,struct file *file);



static ssize_t test_read(struct file *file, char *buf, size_t count, loff_t *f_pos)

{

    int len;

    if (count < 0 )

    {

        return -EINVAL;

    }

    len = strlen(data);

    count = (len > count)?count:len;

    if (copy_to_user(buf, data, count))

    {

        return -EFAULT;

    }

    return count;

}



static ssize_t test_write(struct file *file, const char *buffer, size_t count, loff_t *f_pos)

{

    if(count < 0)

    {

        return -EINVAL;

    }

    memset(data, 0, BUFF_SZ);

    count = (BUFF_SZ > count)?count:BUFF_SZ;

    if (copy_from_user(data, buffer, count))

    {

        return -EFAULT;

    }

    return count;

}



static int test_open(struct inode *inode, struct file *file)

{

    printk("This is open operation\n");

    data = (char*)kmalloc(sizeof(char) * BUFF_SZ, GFP_KERNEL);

    if (!data)

    {

        return -ENOMEM;

    }

    memset(data, 0, BUFF_SZ);

    return 0;

}



static int test_release(struct inode *inode,struct file *file)

{

    printk("This is release operation\n");

    if (data)

    {

        kfree(data);

        data = NULL;

    }

    return 0;

}



static void test_setup_cdev(struct cdev *dev, int minor,

        struct file_operations *fops)

{

    int err, devno = MKDEV(major, minor);

    

    cdev_init(dev, fops);

    dev->owner = THIS_MODULE;

    dev->ops = fops;

    err = cdev_add (dev, devno, 1);

    if (err)

    {

        printk (KERN_NOTICE "Error %d adding test %d", err, minor);

    }

}


static struct file_operations test_fops = 

{

    .owner   = THIS_MODULE,

    .read    = test_read,

    .write   = test_write,

    .open    = test_open,

    .release = test_release,

};



int init_module(void)

{

    int result;

    dev_t dev = MKDEV(major, 0);



    if (major)

    {

        result = register_chrdev_region(dev, 1, TEST_DEVICE_NAME);

    }

    else 

    {

        result = alloc_chrdev_region(&dev, 0, 1, TEST_DEVICE_NAME);

        major = MAJOR(dev);

    }



    if (result < 0) 

    {

        printk(KERN_WARNING "Test device: unable to get major %d\n", major);

        return result;

    }



    test_setup_cdev(&test_dev, 0, &test_fops);

    printk("The major of the test device is %d\n", major);

    return 0;

}

void cleanup_module(void) 

{

    cdev_del(&test_dev);

    unregister_chrdev_region(MKDEV(major, 0), 1);

    printk("Test device uninstalled\n");

}
test.c代码如下：



/* test.c */



#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <sys/stat.h>

#include <sys/types.h>

#include <unistd.h>

#include <fcntl.h>

#define     TEST_DEVICE_FILENAME        "/dev/test_dev"

#define     BUFF_SZ             1024



int main()

{

    int fd, nwrite, nread;

    char buff[BUFF_SZ];



    fd = open(TEST_DEVICE_FILENAME, O_RDWR);

    if (fd < 0)

    {

        perror("open");

        exit(1);

    }

        

    do

    {

        printf("Input some words to kernel(enter 'quit' to exit):");

        memset(buff, 0, BUFF_SZ);

        if (fgets(buff, BUFF_SZ, stdin) == NULL)

        {

            perror("fgets");

            break;

        }

        buff[strlen(buff) - 1] = '\0';

        

        if (write(fd, buff, strlen(buff)) < 0)

        {

            perror("write");

            break;

        }

        

        if (read(fd, buff, BUFF_SZ) < 0)

        {

            perror("read");

            break;

        }

        else

        {

            printf("The read string is from kernel:%s\n", buff);

        }

        

    } while(strncmp(buff, "quit", 4));

    

    close(fd);

    exit(0);

}
