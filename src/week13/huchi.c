#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

void *thread_function(void *arg);
pthread_mutex_t work_mutex;

#define WORK_SIZE 1024
char work_area[WORK_SIZE];
int time_to_exit = 0;                           /* 用来控制是否退出*/

int main() {
  int res;
  pthread_t a_thread;
  void *thread_result;

  res = pthread_mutex_init(&work_mutex,NULL);    /* 初始化一个互斥锁 */
  if (res != 0) {
    perror("Mutex initialization failed");
    exit(EXIT_FAILURE);
  }
  res = pthread_create(&a_thread, NULL, thread_function, NULL);  /* 创建一个新线程 */
  if (res != 0) {
    perror("Thread creation failed");
    exit(EXIT_FAILURE);
  }
  pthread_mutex_lock(&work_mutex);                       /* 尝试对互斥量加锁 */
  printf("Input some text, Enter 'end' to finish\n");
  while(!time_to_exit) {                                   /* 检查是不是该退出*/
    fgets(work_area, WORK_SIZE, stdin);                   /* 从标准输入获取输入到work_area */
    pthread_mutex_unlock(&work_mutex);                   /* 解锁互斥量 */
    while(1) {
      pthread_mutex_lock(&work_mutex);
      if (work_area[0] != '\0') {                      /* 持续检查work_area 是否为空, 如果不为空继续等待，如果为空，则重新读取输入到work_area*/
        pthread_mutex_unlock(&work_mutex);
        sleep(1);
      }
      else {
        break;
      }
    }
  }
  pthread_mutex_unlock(&work_mutex);
  printf("\nWaiting for thread to finish...\n");
  res = pthread_join(a_thread, &thread_result);
  if (res != 0) {
    perror("Thread join failed");
    exit(EXIT_FAILURE);
  }
  printf("Thread joined\n");
  pthread_mutex_destroy(&work_mutex);
  exit(EXIT_SUCCESS);
}

void *thread_function(void *arg) {
  sleep(1);
  pthread_mutex_lock(&work_mutex);                     /* 尝试加锁互斥量 */
  while(strncmp("end", work_area, 3) != 0) {           /* 当work_area里的值不是以end开头时*/
    printf("You input %ld characters\n", strlen(work_area) -1);     /* 输出输入的字符长度 */
    work_area[0] = '\0';                                      /* work_area设置为空 */
    pthread_mutex_unlock(&work_mutex);
    sleep(1);
    pthread_mutex_lock(&work_mutex);
    while (work_area[0] == '\0') {              /* 持续检查work_area 直到它里面有输入值*/
      pthread_mutex_unlock(&work_mutex);
      sleep(1);
      pthread_mutex_lock(&work_mutex);
    }
  }
  time_to_exit = 1;                        /* 当输入end后，设置退出标志 */
  work_area[0] = '\0';
  pthread_mutex_unlock(&work_mutex);
  pthread_exit(0);
}
