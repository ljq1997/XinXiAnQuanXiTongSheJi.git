	.file	"week060320155320.c"
	.section	.rodata
.LC0:
	.string	"v=%d,uv=%u\n"
	.text
	.globl	main
	.type	main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movw	$5320, -4(%rbp)
	movzwl	-4(%rbp), %eax
	movw	%ax, -2(%rbp)
	movzwl	-2(%rbp), %edx
	movswl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	nop
	leave
ret
