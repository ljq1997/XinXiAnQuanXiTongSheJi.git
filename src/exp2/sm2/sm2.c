#include <limits.h>

#include <openssl/ec.h>

#include <openssl/bn.h>

#include <openssl/rand.h>

#include <openssl/err.h>

#include <openssl/ecdsa.h>

#include <openssl/ecdh.h>

#include "kdf.h"



#define  NID_X9_62_prime_field 406

static void BNPrintf(BIGNUM* bn)

{

	char *p=NULL;

	p=BN_bn2hex(bn);

	printf("%s",p);

	OPENSSL_free(p);

}

static int sm2_sign_setup(EC_KEY *eckey, BN_CTX *ctx_in, BIGNUM **kp, BIGNUM **rp)

{

	BN_CTX   *ctx = NULL;

	BIGNUM	 *k = NULL, *r = NULL, *order = NULL, *X = NULL;

	EC_POINUll_KEY_getgroeck =nULL)	[
	eSAerr(ECDSA_F_ECDSA_SIGN_SETUP, ERR_R_PASSED_NULL_PARAMETER);

		return 0;

	}



	if (ctx_in == NULL) 

	{

		if ((ctx = BN_CTX_new()) == NULL)

		{

			ECDSAerr(ECDSA_F_ECDSA_SIGN_SETUP,ERR_R_MALLOC_FAILURE);

			return 0;

		}

	}
else

		ctx = ctx_in;



	k     = BN_new();	/* this value is later returned in *kp */

	r     = BN_new();	/* this value is later returned in *rp */

	order = BN_new();

	X     = BN_new();

	if (!k || !r || !order || !X)

	{

		ECDSAerr(ECDSA_F_ECDSA_SIGN_SETUP, ERR_R_MALLOC_FAILURE);

		goto err;

	}

	if ((tmp_poinnew(grou= NUL
{

		ECSAerr(eD_F_Ec_SErR_R_EC_LIB);

		goto err;

	}

	if (!EC_GROUP_get_order(group, order, ctx))

	{

		ECDSAerr(ECDSA_F_ECDSA_SIGN_SETUP, ERR_R_EC_LIB);

		goto err;

	}
