#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include<string.h>
ino_t get_inode(char* file);//取得文件名对应的i-node
void get_inode_name(ino_t i_node,char *file_name,int length);//取得i-node对应的文件名
void print_direct(ino_t i_node);//打印目录
void main()
{
    ino_t i_node;//i-node指针

    //char *file=".";
   
    print_direct(get_inode("."));
   printf("\n");


}

void print_direct(ino_t i_node)//打印目录
{
    ino_t n_inode;
    char *file_name[256];
 if(get_inode("..")!=get_inode(".")){//判断是否是根目录
      chdir("..");//进入上级目录
      get_inode_name(i_node,file_name,256);
     n_inode=get_inode(".");//更新当前目录的i-node
     print_direct(n_inode);
     printf("/%s",file_name);
}
}

void get_inode_name(ino_t i_node,char *file_name,int length)
//取得i-node 对应的文件名
{
      DIR* dir_ptr;
    struct dirent* direntp;
        dir_ptr = opendir(".");
    while((direntp = readdir(dir_ptr)) != NULL)//当获取下一个目录流进入点失败时返回NULL
    {
          if(direntp->d_ino==i_node)//如果direntp索引节点号与待找的i—node相同时
        {
            strncpy(file_name,direntp->d_name,length);
            file_name[length-1]='\0';
             closedir(dir_ptr);
    }
//     else
  //          printf("failed to find the name of i-node");
       }

}


ino_t get_inode(char* file)//取得文件名对应的i-node
{
struct stat buf;
if(stat(file,&buf)!=-1)//执行成功
{
   return buf.st_ino;
}
else{
   printf("failed to get inode");
 }
}

