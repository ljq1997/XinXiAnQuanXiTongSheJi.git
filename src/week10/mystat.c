#include <sys/stat.h>  
#include <unistd.h>  
#include <stdio.h>  
  
int main() {  
    struct stat buf;  
    stat("test", &buf);  
    printf("the file size = %d\n", buf.st_size);  
}  
